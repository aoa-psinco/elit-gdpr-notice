<?php

/**
 * Plugin Name: Elit GDPR Notice
 * Author: Patrick Sinco
 * Description: Display an opt-in notice that complies with GDPR requirements
 * Version: 0.1.0
 */

if ( !defined( 'WPINC' ) ) {
  die;
}

function elit_gdpr_load_script() {
  if ( ! is_admin() ) {
    wp_enqueue_script(
      'elit-gdpr-script',
      plugin_dir_url( __FILE__ ) . 'public/scripts/elit-gdpr.js',
      array(),
      false,
      true
    );
  }
}
add_action( 'wp_enqueue_scripts' , 'elit_gdpr_load_script' );
