document.addEventListener('DOMContentLoaded', function() {
  if (! document.cookie.split(';').filter(function (item) { 
    return item.indexOf('cookie-notice-accepted=') >= 0 
  }).length) {
    var privacyLink = document.createElement('a');
    var privacyLinkText = document.createTextNode('our privacy policy');
    privacyLink.appendChild(privacyLinkText);
    privacyLink.title = 'Privacy Policy';
    privacyLink.href = 'http://www.osteopathic.org/inside-aoa/about/Pages/privacy-policy.aspx';
    privacyLink.style.color = '#4e98da';

    var acceptLink = document.createElement('a');
    var acceptLinkText = document.createTextNode('Accept');
    acceptLink.appendChild(acceptLinkText);
    acceptLink.title = 'Accept';
    acceptLink.style.color = '#4e98da';
    acceptLink.style.cursor = 'pointer';

    var notice = document.createElement('div');
    notice.style.display = 'block';
    notice.style.position = 'fixed';
    notice.style.bottom = '0';
    notice.style.left = '0';
    notice.style.right = '0';
    notice.style.zIndex = '5000';
    notice.style.fontFamily = 'inherit';
    notice.style.padding = '16px';
    notice.style.textAlign = 'center';
    notice.style.lineHeight = '1.4';
    notice.style.fontSize = '16px';
    notice.style.backgroundColor = '#f5f5f5';
    notice.style.color = '#667';
    notice.style.opacity = '0.95';

    var text = document.createTextNode('This site uses cookies. By continuing to use our website, you are agreeing to ')
    notice.appendChild(text);
    notice.appendChild(privacyLink);
    notice.appendChild(document.createTextNode('. | '));
    notice.appendChild(acceptLink);
    document.body.appendChild(notice);

    acceptLink.addEventListener('click', function(evt) {
      var d = new Date();
      var year = d.getFullYear();
      var month = d.getMonth();
      var day = d.getDate();
      var oneYearFromNow = new Date(year + 1, month, day);
      document.cookie = 'cookie-notice-accepted=true;expires=' + 
        oneYearFromNow.toUTCString()

      document.body.removeChild(notice);
    });
  }
});
